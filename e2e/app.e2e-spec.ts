import { FlowupMinesweeperPage } from './app.po';

describe('flowup-minesweeper App', () => {
  let page: FlowupMinesweeperPage;

  beforeEach(() => {
    page = new FlowupMinesweeperPage();
  });

  it('deberia existir un titulo GH', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('GH');
  });
});
