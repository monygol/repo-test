import { Component, OnInit } from '@angular/core';

export interface ResultElement {
  dificulty: string;
  position: number;
  startTime: number;
  endTime: number;
  timeSpent: number;
  status: string;
}

const ELEMENT_DATA: ResultElement[] = [
  {position: 1, endTime: 0,startTime: 0 ,dificulty: 'easy', timeSpent: 1.0079, status: 'won'},
  {position: 2, endTime: 0,startTime: 0 ,dificulty: 'easy', timeSpent: 4.0026, status: 'won'},
  {position: 3, endTime: 0,startTime: 0 ,dificulty: 'easy', timeSpent: 6.941, status: 'won'},
  {position: 4, endTime: 0,startTime: 0 ,dificulty: 'hard', timeSpent: 9.0122, status: 'won'},
  {position: 5, endTime: 0,startTime: 0 ,dificulty: 'hard', timeSpent: 10.811, status: 'won'},
  {position: 6, endTime: 0,startTime: 0 ,dificulty: 'hard', timeSpent: 12.0107, status: 'lost'},
  {position: 7, endTime: 0,startTime: 0 ,dificulty: 'hard', timeSpent: 14.0067, status: 'lost'},
  {position: 8, endTime: 0,startTime: 0 ,dificulty: 'hard', timeSpent: 15.9994, status: 'lost'},
  {position: 9, endTime: 0,startTime: 0 ,dificulty: 'hard', timeSpent: 18.9984, status: 'lost'},
  {position: 10, endTime: 0,startTime: 0 ,dificulty: 'medium', timeSpent: 20.1797, status: 'lost'},
];


@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit {

  dataSource = ELEMENT_DATA;

  constructor() { }

  ngOnInit() {
  }

}