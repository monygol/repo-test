import { TestBed, inject } from '@angular/core/testing';

import { GameService } from './game.service';

describe('GameService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GameService]
    });
  });

  it('deberia haber un servicio', inject([GameService], (service: GameService) => {
    expect(service).toBeTruthy();
  }));
});
